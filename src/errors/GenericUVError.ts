import UVErrorBase, { UVErrors } from "./UVError";

// this doesn't extend UVErrorBase, because we return an instance of a temporary class
export default class GenericUVError {
    constructor(_code: UVErrors, _message: string) {
        class temp extends UVErrorBase {
            static readonly code = _code;
            static readonly message = _message;
        }
        return new temp();
    }
}
