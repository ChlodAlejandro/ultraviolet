import Log from "app/data/AppLog";

export default async function (text: string): Promise<boolean> {
    const methods = [
        async (): Promise<void> => {
            return navigator.clipboard.writeText(text);
        },
        async (): Promise<void> => {
            const textarea = document.createElement("textarea");
            textarea.value = text;
            textarea.style.position = "fixed";
            textarea.style.left = "-100vw";
            textarea.style.top = "-100vh";
            document.body.appendChild(textarea);
            textarea.select();
            // noinspection JSDeprecatedSymbols
            document.execCommand("copy");
            document.body.removeChild(textarea);
        },
    ];

    for (const method of methods) {
        try {
            await method();
        } catch (e) {
            if (methods[methods.length - 1] !== method) continue;
            Log.error("Could not copy to clipboard.", { e });
            return false;
        }
    }
    return true;
}
