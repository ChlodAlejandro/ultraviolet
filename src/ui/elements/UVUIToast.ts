import random from "app/util/random";
import UVUIElement, { UVUIElementProperties } from "./UVUIElement";
import UltravioletUI from "app/ui/UltravioletUI";

export interface UVUIToastProperties extends UVUIElementProperties {
    content: string;
    action?: UVUIToastAction;
    /**
     * Time, in milliseconds, for the toast to automatically close. Must be
     * between 4000 and 10000.
     * @default 5000
     */
    timeout?: number;
    /**
     * Used to track displayed dialogs.
     * @internal
     */
    id?: string;
    style?: UVUIToastStyle;
}

export interface UVUIToastAction {
    callback: () => any;
    text: string;
}

export enum UVUIToastStyle {
    /** The default. Centered on the bottom of the screen, one line only. */
    Normal = 0,
    /** On the leading edge of the screen (left in LTR, right in RTL) instead of centered. */
    Leading,
    /** Positions the button under the text */
    Stacked,
}

export class UVUIToast extends UVUIElement {
    public static readonly elementName = "uvToast";

    /**
     * A unique identifier for this toast, to allow multiple active toasts.
     */
    id: string;

    /**
     * The HTMLDivElement which contains the actual toast.
     */
    element?: HTMLDivElement;

    protected constructor(readonly props: UVUIToastProperties) {
        super();
        this.id = `toast__${props.id || random(16)}`;
        this.props.style ??= UVUIToastStyle.Normal;
    }

    /**
     * Helper function to create and instantly show a toast.
     */
    static quickShow(props: UVUIToastProperties): Promise<void> {
        const toast = new UltravioletUI.Toast(props);
        return toast.show();
    }

    /**
     * Shows the toast.
     */
    show(): Promise<void> {
        throw new Error("Attempted to call abstract method");
    }

    /**
     * Renders the toast. This only creates the toast body, and does not show
     * it.
     */
    render(): HTMLDivElement {
        throw new Error("Attempted to call abstract method");
    }
}
