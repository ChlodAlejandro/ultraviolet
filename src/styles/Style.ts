import {
    UltravioletHook,
    UltravioletHookEventTypes,
} from "app/event/UltravioletHookEvent";
import { Dependency } from "app/data/Dependencies";
import { UVUIElements } from "app/ui/UltravioletUI";
import i18next from "i18next";

type URLString = string;

interface Style {
    name: string;
    version: string;

    // Mapped by language.
    meta?: {
        [key: string]: {
            displayName?: string;
            description?: string;
            author?: string | string[]; // Must be wiki usernames

            homepage?: URLString; // URL

            repository?: URLString; // URL
            issues?: URLString; // URL

            banner?: URLString; // URL of Image
        };
    };

    dependencies?: Dependency[];
    storage?: Record<string, any>;

    classMap: {
        [T in keyof typeof UVUIElements]: typeof UVUIElements[T];
    };

    hooks?: { [key in UltravioletHookEventTypes]?: UltravioletHook[] };
}

export default Style;

export abstract class StyleStorage {}

export function getStyleMeta(style: Style): Style["meta"][string] {
    return (
        style.meta[i18next.language ?? "en-US"] ??
        style.meta["en-US"] ??
        Object.values(style.meta)[0]
    );
}
